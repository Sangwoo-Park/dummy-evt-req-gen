import { Request, Response, NextFunction } from 'express';
import { Controller, Post } from '@sangwoo/controller-decorators';
import { KafkaClient } from '@sangwoo/common';

import { EvaluateMercuryProducer } from '../utils/EvaluateMercuryProducer';

@Controller('')
class GenController {
    @Post('/gen')
    async postGenerate(req: Request, res: Response, next: NextFunction) {
        const producer = new EvaluateMercuryProducer(KafkaClient.getInstance().kafka);
        await producer.connect();

        const request = {
            continuousTraining: '5ffbdc6602cb870d1275dfd4',
            round: 99,
            validateModel: false,
            modelId: '60006236e199e200193036ec',
            imageId: '5fd18797afff7300185c8623',
            modelPath: 'daeduck/models/6007cc108379830019195332-100.dat',
            imagePath: 'daeduck/images/5fd18797afff7300185c85f1.png'
        };

        for (let i = 0; i < 900; i++) {
            await producer.send(request);
        }

        res.send('Requests have been sent.');
    }
}
