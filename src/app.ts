import express from 'express';
import { requestLogger } from '@sangwoo/logger';
import { AppRouter } from '@sangwoo/controller-decorators';

import './controllers/GenController';

const app = express();

app.use(requestLogger);

app.use(express.json());

app.use(AppRouter.getInstance());

export default app;
