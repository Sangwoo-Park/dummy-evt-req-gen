# Dummy Evaluation Requests Generator
## Summary
이 프로그램은 test.req.evaluateMercury topic에 dummy 요청을 900개 기록합니다.

## How to use
1. 코드를 clone 합니다.
1. `npm install`을 실행합니다.
1. `npm start`로 실행합니다.
1. POST `localhost:4000/gen` 요청을 전송합니다.
