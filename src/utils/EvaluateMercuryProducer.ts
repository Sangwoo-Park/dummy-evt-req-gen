import { BaseProducer } from '@sangwoo/common';

interface IEvaluateMercuryReqRecord {
    key: string;
    value: {
        continuousTraining: string;
        round: number;
        validateModel: boolean;
        modelId: string;
        imageId: string;
        modelPath: string;
        imagePath: string;
    };
}

class EvaluateMercuryProducer extends BaseProducer<IEvaluateMercuryReqRecord> {
    topic = 'test.req.evaluateMercury';
}

export { EvaluateMercuryProducer };
