import { AddressInfo } from 'net';
import { logger } from '@sangwoo/logger';

import app from './app';

import 'dotenv/config';

(async () => {
    try {
        const server = app.listen(process.env.SERVER_PORT, () => {
            const { port } = server.address() as AddressInfo;

            logger.info(`Listening on port ${port}`);
        });
    }
    catch (e) {
        logger.error(`Can not start server: ${e.message}`);
    }
})();
